<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminRequest;
use App\Models\User;
use App\Repositories\AdminRepository;
use Illuminate\Http\Request;
class AdminController extends Controller
{
    protected $AdminRepository;

    public function __construct(AdminRepository $AdminRepository)
    {
        $this->AdminRepository = $AdminRepository;
    }

    public function index(Request $request)
    {
        try {
            $data = $request->id === null ? $this->AdminRepository->all() : $this->AdminRepository->all($request->id);
            for($i=0; $i<count($data); $i++){
                $data[$i]->password = '';
            }
            return view('masterAdmin.index', ['data' => $data] );
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function store(AdminRequest $request)
    {
        try {
            $data = $this->AdminRepository->storeOrUpdate($request);
            
            return redirect()->back()->with('success', 'Admin Edit successfully.');
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function update(AdminRequest $request, $id)
    {
        $request->id = $id;
        // dd($request);
        try {
            $data = $this->AdminRepository->storeOrUpdate($request, 'PUT');
            return redirect()->back()->with('success', 'Admin Edit successfully.');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }

    public function delete($id)
    {
        try {
            $this->AdminRepository->destroy($id);
            return redirect()->back()->with('success', 'Admin deleted successfully.');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', $e->getMessage());
        }
    }
}


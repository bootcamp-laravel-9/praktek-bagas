<?php

namespace App\Http\Controllers;

use App\Http\Requests\AuthRequest;
use App\Models\User;
use App\Repositories\AuthRepository;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    protected $AuthRepository;
    public function __construct(AuthRepository $AuthRepository)
    {
        $this->AuthRepository = $AuthRepository;
    }

    public function loginForm()
    {
        return view('auth/loginForm');
    }

    public function login(AuthRequest $request)
    {
        try {
            $user = $this->AuthRepository->login($request);
            // dd($user);
            return $user;
        } catch (\Exception $e) {
            return response()->json([
                'status' => false,
                'message' => $e->getMessage(),
            ], 400);
        }
    }

    public function attemptLogin(AuthRequest $request) {
        $request = Request::create(url('api/login'), 'POST', [
            'email' => $request->email,
            'password' => $request->password,
        ]);

        $request->headers->set('Accept', 'application/json');
        
        $response = app()->make('router')->dispatch($request);

        if($response->getStatusCode() == 200) {
            return redirect('/')->with('success', 'Login Berhasil');
        }else{
            return redirect('/loginForm')->with('error', 'Login Gagal');
        }
    }

    public function logout(Request $request)
    {
        $request->session()->flush();
        return redirect('/loginForm');
    }
}

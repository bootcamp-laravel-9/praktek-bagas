<?php

namespace App\Http\Controllers;

use App\Models\ticket_category;
use App\Models\ticket_detail;
use App\Models\ticket_header;
use App\Repositories\TicketRepository;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;
use Illuminate\Validation\Rule;

class TicketDetailController extends Controller
{
    public function index()
    {
        try {
            $data = DB::select(
                DB::raw(" 
                    select b.no_tiket, b.nama, b.email, b.no_telp, c.name, b.date_ticket, a.total_ticket FROM ticket_details a
                    INNER JOIN ticket_headers b ON a.ticket_header_id = b.id
                    INNER JOIN ticket_categories c ON a.ticket_categories_id = c.id"
                )
            );
            return response()->json(['success' => true, 'data' => $data]);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
    }
    public function indexCreate()
    {
        return view('ticket.create');
    }

    public function consume(Request $request)
    {
        try {
            $response = $this->index();
            $data = json_decode($response->getContent());
            $dataInsert = [];
            foreach ($data->data as $item) {
                $dataInsert[] = (object) [
                    'no_tiket' => $item->no_tiket,
                    'nama' => $item->nama,
                    'email' => $item->email,
                    'no_telp' => $item->no_telp,
                    'name' => $item->name,
                    'date_ticket' => $item->date_ticket,
                    'total_ticket' => $item->total_ticket,
                ];
            }
            // dd($dataInsert);
            return view('ticket.index', ['data' => $dataInsert]);

        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
    }

    public function consumeStore(Request $request)
    {
        try {
            $validatedData = $request->validate([
                'no_tiket' => 'required|unique:ticket_headers,no_tiket',
                'nama' => 'required|string',
                'email' => 'required|email',
                'no_telp' => 'required|string',
                'kategori' => 'required|exists:ticket_categories,name',
                'date_ticket' => 'required|date',
                'total_ticket' => 'required|integer|min:1',
            ]);

            $addres = $request->input('addres', null);

            $ticketHeader = ticket_header::create([
                'no_tiket' => $validatedData['no_tiket'],
                'nama' => $validatedData['nama'],
                'email' => $validatedData['email'],
                'addres' => $addres,
                'no_telp' => $validatedData['no_telp'],
                'date_ticket' => $validatedData['date_ticket'],
            ]);

            $ticketCategory = ticket_category::where('name', $validatedData['kategori'])->firstOrFail();
            
            $ticketDetail = ticket_detail::create([
                'ticket_header_id' => $ticketHeader->id,
                'ticket_categories_id' => $ticketCategory->id,
                'total_ticket' => $validatedData['total_ticket'],
            ]); 
            $ticketDetail->save();
            return response()->json(['success' => true, 'message' => 'Ticket detail created successfully', 'data' => $ticketDetail], 201);
        } catch (\Exception $e) {
            return response()->json(['success' => false, 'message' => $e->getMessage()], 500);
        }
    }

    public function attemptTickets(Request $request)
    {
        $request = Request::create(url('api/createTicketsApi'), 'POST', [
            'no_tiket' => $request->no_tiket,
            'nama' => $request->nama,
            'email' => $request->email,
            'no_telp' => $request->no_telp,
            'addres' => $request->addres,
            'kategori' => $request->kategori,
            'date_ticket' => $request->date_ticket,
            'total_ticket' => $request->total_ticket,
        ]);
        // dd($request);
        $request->headers->set('Accept', 'application/json');
        
        $response = app()->make('router')->dispatch($request);
        dd($response);
        if($response->getStatusCode() == 200) {
            return redirect('/ticket.index')->with('success', 'create ticket Berhasil');
        }else{
            return redirect('/loginForm')->with('error', 'Login Gagal');
        }
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ticket_header extends Model
{
    use HasFactory;
    protected $fillable = [
        'ticket_header_id',
        'no_tiket',
        'nama',
        'email',
        'no_telp',
        'addres'
    ];

    public function ticketDetails()
    {
        return $this->hasMany(ticket_detail::class);
    }
}

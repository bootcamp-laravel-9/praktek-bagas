<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ticket_detail extends Model
{
    use HasFactory;

    protected $fillable = [
        'ticket_header_id',
        'ticket_categories_id',
        'total_ticket',
    ];


    public function ticketHeader()
    {
        return $this->belongsTo(ticket_header::class);
    }

    public function ticketCategory()
    {
        return $this->belongsTo(ticket_detail::class);
    }
}

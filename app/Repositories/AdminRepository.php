<?php

namespace App\Repositories;
use App\Http\Resources\AdminResource;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class AdminRepository
{
    protected $user;

    public function __construct(User $user){
        $this->user = $user;
    }

    public function all ($id = null)
    {
        if ($id == null) {
            $response = AdminResource::collection($this->user->all());
            return $response;
        }

        $admin = $this->user->find($id);
        if (!$admin) {
            throw new \Exception("Data kendaraan tidak ditemukan.", 400);
        }

        $response = new AdminResource($admin);
        return $response;
    }
    
    public function storeOrUpdate($request, $method = null)
    {
        if ($method == 'PUT' && !$this->user->find($request->id)) {
            throw new \Exception("Data admin tidak ditemukan.", 400);
        }
        
        $data = $this->user->updateOrCreate(
            ['id' => $request->id ?? null],
            [
                'name' => $request->name,
                'email' => $request->email,
                'password' => Hash::make($request->password),
            ]
        );
        
        return $data;
    }

    public function destroy($id)
    {
        $admin = $this->user->find($id);

        if (!$admin) throw new \Exception('Data Admin tidak ditemukan', 404);

        $admin->delete();
    }
}
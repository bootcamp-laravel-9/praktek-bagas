<?php

namespace App\Repositories;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthRepository
{
    protected $user;

    public function __construct(User $user){
        $this->user = $user;
    }

    public function login($request)
    {
        $credentials = $request->only('email', 'password');
        // dd($credentials);

        if (!Auth::attempt($credentials)) {
            // dd('salah');
            return response()->json([
                'status' => 'false',
                'message' => 'Login Failed, please check your credential'
            ],400);
        }

        // dd('benar');
        $user = User::where('email', $credentials['email'])->first();
        $token = $user->createToken(config('app.name'))->plainTextToken;
        session()->put('LoginSession',$token);
        // dd($token);
        // $request->session()->put('user', $user);
        
        return response()->json([
            'status' => true,
            'message' => 'Login succesfully.',
            'data' => $user
        ],200);
    }
    

}
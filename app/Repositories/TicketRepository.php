<?php

namespace App\Repositories;
use App\Models\ticket_detail;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class TicketRepository
{
    protected $ticket;

    public function __construct(ticket_detail $ticket){
        $this->ticket_detail = $ticket;
    }
}
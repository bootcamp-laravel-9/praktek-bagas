<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketHeaderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Insert sample data into the ticket_headers table
        DB::table('ticket_headers')->insert([
            [
                'no_tiket' => 'TKT001',
                'nama' => 'John Doe',
                'email' => 'john@example.com',
                'no_telp' => '123456789',
                'addres' => '123 Main St, City',
                'date_ticket' => '2024-04-10',
                'created_at' => now(),
                'updated_at' => now(),
            ],
            [
                'no_tiket' => 'TKT002',
                'nama' => 'Jane Smith',
                'email' => 'jane@example.com',
                'no_telp' => '987654321',
                'addres' => '456 Elm St, Town',
                'date_ticket' => '2024-04-12',
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TicketDetailSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ticket_details')->insert([
            [
                'ticket_header_id' => 2,
                'ticket_categories_id' => 2,
                'total_ticket' => 15,
                'created_at' => now(),
                'updated_at' => now(),
            ],
        ]);
    }
}

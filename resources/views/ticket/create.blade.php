@extends('layout/main') 
@section('menu-ticket', 'active') 
@section('title', 'Master Ticket')
@section('content')

<div class="row">
    <div class="col-md-6">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Create Ticket</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
                <form action="{{ url('/attemptTickets') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="no_tiket">Ticket Number</label>
                        <input type="text" class="form-control" id="no_tiket" name="no_tiket" required>
                    </div>
                    <div class="form-group">
                        <label for="nama">Name</label>
                        <input type="text" class="form-control" id="nama" name="nama" required>
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email" name="email" required>
                    </div>
                    <div class="form-group">
                        <label for="no_telp">Phone Number</label>
                        <input type="text" class="form-control" id="no_telp" name="no_telp" required>
                    </div>
                    <div class="form-group">
                        <label for="category">Category</label>
                        <select class="form-control" id="category" name="kategori" required>
                            <option value="kategori1">kategori1</option>
                            <option value="kategori2">kategori2</option>
                            <option value="kategori3">kategori3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="date_ticket">Ticket Date</label>
                        <input type="date" class="form-control" id="date_ticket" name="date_ticket" required>
                    </div>
                    <div class="form-group">
                        <label for="total_ticket">Total Ticket</label>
                        <input type="number" class="form-control" id="total_ticket" name="total_ticket" required>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
</div>

@endsection

@extends('layout/main') 
@section('menu-admin', 'active') 
@section('title', 'Master Admin')
@section('content')

<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-header">
                <h3 class="card-title">Tabel Admin</h3>
                <div class="card-tools">
                    <button onclick="$('#addAdminModal').modal('show')" class="btn btn-success btn-sm">+ Tambah Admin</button> 
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama</th>
                            <th>Email</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $row['name'] }}</td>
                            <td>{{ $row['email'] }}</td>
                            <td class="d-flex ">
                               <button onclick="openEditModal('{{ $row->id }}', '{{ $row->name }}', '{{ $row->email }}', '{{ $row->password }}' )" class="btn btn-info btn-sm ">Edit</button> 
                               <form action="{{ url('/deleteAdmin/' . $row->id) }}" method="GET" class="mx-2">
                                    @csrf
                                    <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Create Modal -->
<div class="modal fade" id="addAdminModal" tabindex="-1" role="dialog" aria-labelledby="addAdminModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="addAdminModalLabel">Tambah Admin</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
            <form action="{{ url('/createAdmin') }}" method="post">
                @csrf
                <div class="form-group">
                    <label for="name">Nama:</label>
                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ old('name') }}" required>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="email">Email:</label>
                    <input type="email" class="form-control @error('email') is-invalid @enderror" id="email" name="email" value="{{ old('email') }}" required>
                    @error('email')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                    <label for="password">Password:</label>
                    <input type="password" class="form-control @error('password') is-invalid @enderror" id="password" name="password" required>
                    @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
          </div>
      </div>
  </div>
</div>

<!-- Edit Modal -->
<div class="modal fade" id="editAdminModal" tabindex="-1" role="dialog" aria-labelledby="editAdminModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
      <div class="modal-content">
          <div class="modal-header">
              <h5 class="modal-title" id="addAdminModalLabel">Edit Admin</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
              </button>
          </div>
          <div class="modal-body">
              <form id="editAdminForm" action="" method="post">
                  @csrf
                  @method('PUT') 
                  <div class="form-group">
                      <label for="name">Nama:</label>
                      <input type="text" class="form-control" id="name" name="name" >
                  </div>
                  <div class="form-group">
                      <label for="email">Email:</label>
                      <input type="email" class="form-control" id="email" name="email" >
                  </div>
                  <div class="form-group">
                      <label for="password">Password:</label>
                      <input type="text" class="form-control" id="password" name="password" >
                  </div>
                  <button type="submit" class="btn btn-primary">Submit</button>
              </form>
          </div>
      </div>
  </div>
</div>

<script>
    function openEditModal(id, name, email, password) {
        $('#editAdminModal #name').val(name);
        $('#editAdminModal #email').val(email);
        $('#editAdminModal #password').val(password);
        $('#editAdminModal #id').val(id);

        $('#editAdminModal #editAdminForm').attr('action', '/editAdmin/' + id);
        
        $('#editAdminModal').modal('show');
    }
</script>

@endsection

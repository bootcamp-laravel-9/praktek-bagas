<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => ['guest']], function () {
    Route::post('attemptLogin', 'AuthController@attemptLogin')->name('attemptLogin');
    Route::get('loginForm', 'AuthController@loginForm')->name('LoginForm');
});

Route::group(['middleware' => ['session.login']], function () {
    Route::get('/', function () {
        return view('dashboard/index');
    })->name('dashboard');

    Route::post('/createAdmin', 'AdminController@store')->name('createAdmin');
    Route::put('/editAdmin/{id}', 'AdminController@update')->name('editAdmin');
    Route::get('/masterAdmin', 'AdminController@index')->name('masterAdmin');
    Route::get('/logout', 'AuthController@logout')->name('logout');
    Route::get('/deleteAdmin/{id}', 'AdminController@delete')->name('deleteAdmin');

    Route::get('tickets', 'TicketDetailController@consume')->name('ticketView');
    Route::get('createTickets', 'TicketDetailController@indexCreate')->name('createTickets');
    Route::post('attemptTickets', 'TicketDetailController@attemptTickets')->name('attemptTickets');
});

